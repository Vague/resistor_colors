# Isaac H
# interactive resistor color learner

# https://www.hobby-hour.com/electronics/resistorcalculator.php
# 4 band resistors: digit digit multipler tolerance
# 3 band resistors: digit digit multipler 20% tolerance


from random import *
print(randint(1, 100))    # Pick a random number between 1 and 100.

colors = [ "black", "brown" , "red"  , "orange"  , "yellow"  , "green"  , "blue"  , "violet"  , "grey"  , "white", "Gold", "Silver" ]
tolerance_values = [ 1, 2, 0.5, 0.25, 0.1, 0.05, 5, 10, 20 ]
tolerance_colors = [ "brown", "red", "green", "blue", "violet", "grey", "gold", "silver", "" ]


class Resistor:
	
	def __init__(self):
		self.roll_new_resistor()

	def roll_new_resistor(self):
		#print("new resistor")
		# pick 2 random colors representing the first digits
		self.color_slots = [randint(0,9)]
		self.color_slots += [randint(0,9)]
		# pick another color representing the multiplier
		self.color_slots += [randint(0,11)] # subtract 2 for multipler x10^this
		# pick a random number for the last tolerance band. 
		self.color_slots += [randint(0,8)]
		#print('new resistor: ' + str(self.color_slots) )

			
	def get_ohms(self):
		print("get ohms")
		multiplicand = int(str(self.color_slots[0]) + str(self.color_slots[1]))
		power10 = self.color_slots[2]
		tolerance = tolerance_values[self.color_slots[3]]
		print( str(multiplicand) + ' x10^ ' + str(power10) + ' +- ' + str(tolerance) + ' %')

	def get_colors(self):
		print("get colors")
		#print(colors[self.color_slots[0]])
		#print(colors[self.color_slots[1]])
		#print(colors[self.color_slots[2]])
		#print(tolerance_colors[self.color_slots[3]])

		print( colors[self.color_slots[0]] + ' ' + colors[self.color_slots[1]] + ' ' + colors[self.color_slots[2]] + ' ' + tolerance_colors[self.color_slots[3]] )

# TODO make a screen that displays how to resistor color codes
# TODO use the resistor class to output the color codes, then wait 2s then output the ohms. 

mr = Resistor()
for a in range(4):
	mr.roll_new_resistor()
	print()
	mr.get_colors()
	mr.get_ohms()
	print()

